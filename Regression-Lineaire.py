import matplotlib.pyplot as plt
import csv

dataFile = open('Data.csv', newline='')
dataReader = csv.reader(dataFile, delimiter=';')

x = []
y = []
for data in dataReader:
    x.append(int(data[0]))
    y.append(int(data[1]))

a = 1
b = 1
alpha = 0.001
erreurMoyenne = 2

while erreurMoyenne > 1:
    # print("Valeur de a :", a)
    # print("Valeur de b :", b)

    for i in range(0, len(x)):
        erreurMoyenne += (a * x[i] + b - y[i]) * (a * x[i] + b - y[i]) # MSE = Min Suared Error
    erreurMoyenne *= (1 / (2 * len(x)))
    # print("Erreur moyenne :", erreurMoyenne)

    aPartiel = 0
    bPartiel = 0
    for i in range(0, len(x)):
        aPartiel += x[i] * (a * x[i] + b - y[i])
        bPartiel += 1 * (a * x[i] + b - y[i])
    aPartiel *= (1 / len(x))
    bPartiel *= (1 / len(x))
    # print("Dérivée partielle en a", aPartiel)
    # print("Dérivée partielle en b", bPartiel)

    a -= alpha * aPartiel
    b -= alpha * bPartiel
    # plt.plot([b, a * max(x) + b])

print("Valeur de a :", a)
print("Valeur de b :", b)
print("Erreur moyenne :", erreurMoyenne)

plt.plot(x, y, 'ro', markersize=2)
plt.plot([0, max(x)], [b, a * max(x) + b])
plt.show()
