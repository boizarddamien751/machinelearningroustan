from sklearn import linear_model
from sklearn.preprocessing import PolynomialFeatures
import numpy as np
import matplotlib.pyplot as plt
import csv


# Colonne 0 : Distance réelle en mm
# Colonnes 1 à 6 : Données mesurées en microsecondes (temps pris par le rayon infrarouge pour être mesuré)

x = []
Y = []
with open('DistanceSensorData.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    for row in csv_reader:
        for index in range(1, len(row)):
            if float(row[index].replace(",", ".")) == 0.0: # élimination des données contraignante en 0
                continue
            if float(row[index].replace(",", ".")) > 1800.0: # élimination des données incorrectes supérieures à 1800
                continue

            Y.append(float(row[0].replace(",", ".")))
            x.append(float(row[index].replace(",", ".")))

X = np.array(x).reshape(-1, 1)
Y = np.array(Y)

poly = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly.fit_transform(X)

reg = linear_model.LinearRegression()
reg.fit(X_poly, Y)

print("Paramètres :")
print("f(x) = y =", reg.intercept_, "+ x *", reg.coef_[0], "+ x^2 *", reg.coef_[1])
print("Prédiction :")
print("f(1000) =", reg.intercept_ + 1000.0*reg.coef_[0] + 1000.0*1000.0*reg.coef_[1], "mm")
print("f(1200) =", reg.intercept_ + 1200.0*reg.coef_[0] + 1200.0*1200.0*reg.coef_[1], "mm")
print("f(1400) =", reg.intercept_ + 1400.0*reg.coef_[0] + 1400.0*1400.0*reg.coef_[1], "mm")
print("f(1600) =", reg.intercept_ + 1600.0*reg.coef_[0] + 1600.0*1600.0*reg.coef_[1], "mm")

plt.plot(x, Y, 'ro', markersize=2)

xDisplay = np.linspace(np.min(x), np.max(x), 10)
yDisplay = reg.intercept_ + xDisplay * reg.coef_[0] + xDisplay * xDisplay * reg.coef_[1]
plt.plot(xDisplay, yDisplay)

plt.show()


# x1 = [0, 1, 2]
# x2 = [0, 0, 0]
# X = np.array([x1, x2]).transpose()
#
# Y = np.array([0, 0, 2])
#
# reg = linear_model.LinearRegression()
# reg.fit(X, Y)
# print(reg.coef_)
# print(reg.intercept_)
#
# fig = plt.figure()
# ax = plt.axes(projection='3d')
#
# ax.plot3D(x1, x2, Y, 'ro', markersize=2)
#
# x1Display = np.linspace(np.min(x1), np.max(x1), 10)
# x2Display = np.linspace(np.min(x2), np.max(x2), 10)
# yDisplay = reg.intercept_ + x1Display * reg.coef_[0] + x2Display * reg.coef_[1]
#
# print(x1Display)
# print(x2Display)
# print(yDisplay)
#
# ax.plot3D(x1Display, x2Display, yDisplay)
#
# plt.show()



