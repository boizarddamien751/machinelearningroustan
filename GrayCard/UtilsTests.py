import cv2
import easyocr
import numpy as np
from enum import Enum


def LevenshteinDistance(a, b, memo=None):
    if memo is None:
        memo = {}

    if (a, b) in memo:
        return memo[(a, b)]

    if a == "":
        result = len(b)
    elif b == "":
        result = len(a)
    else:
        cost = 0 if a[-1] == b[-1] else 1
        result = min([
            LevenshteinDistance(a[:-1], b, memo) + 1,
            LevenshteinDistance(a, b[:-1], memo) + 1,
            LevenshteinDistance(a[:-1], b[:-1], memo) + cost
        ])

    memo[(a, b)] = result
    return result


def drawRectanglesWithAI(image):
    readerOCR = easyocr.Reader(['fr'], gpu=True)
    results = readerOCR.readtext(image)  # test_substrate[50:121, 19:126]

    for info in results:
        if isinstance(info[0][0][0], float) or isinstance(info[0][0][1], float) or isinstance(info[0][2][0], float) or isinstance(info[0][2][1], float):
            continue
        image = cv2.rectangle(image, info[0][0], info[0][2], (0, 255, 0), 1)
        # image = cv2.putText(image, info[1], info[0][3], cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1, cv2.LINE_AA)


class DescriptorType(Enum):
    SIFT = 1
    KAZE = 2
    ORB = 3
    SURF = 3


class MatchType(Enum):
    NORMAL = 1
    KNN = 2


def homographyTransformation(origImage, endImage, matchPointNumber: int, descriptor: DescriptorType, matcher: MatchType):
    grayStart = cv2.cvtColor(origImage, cv2.COLOR_BGR2GRAY)
    grayEnd = cv2.cvtColor(endImage, cv2.COLOR_BGR2GRAY)

    # TODO : Mieux comprendre les algorithmes de détection et d'apparairage et notamment leurs paramètres
    try:
        if descriptor == DescriptorType.SIFT:
            descriptorAlgorithm = cv2.SIFT_create()
        elif descriptor == DescriptorType.KAZE:
            descriptorAlgorithm = cv2.KAZE_create()
        elif descriptor == DescriptorType.ORB:
            descriptorAlgorithm = cv2.ORB_create()
        elif descriptor == DescriptorType.SURF:
            descriptorAlgorithm = cv2.SURF_create()
        else:
            return None, None

        kpStart, desStart = descriptorAlgorithm.detectAndCompute(grayStart, None)
        kpEnd, desEnd = descriptorAlgorithm.detectAndCompute(grayEnd, None)
    except cv2.error as e:
        raise e

    # TODO : Chercher d'autres algorithmes d'apparairage de point entre deux images et approfondir l'utilisation de ceux-ci
    if matcher == MatchType.NORMAL:
        bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=True)
        matches = bf.match(desStart, desEnd)
        goodMatches = sorted(matches, key=lambda x: x.distance)
    elif matcher == MatchType.KNN:
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(np.asarray(desStart, np.float32), np.asarray(desEnd, np.float32), k=2)

        goodMatches = []
        for m, n in matches:
            if m.distance < 0.7 * n.distance:
                goodMatches.append(m)
        goodMatches = sorted(goodMatches, key=lambda x: x.distance)
    else:
        return None, None

    if len(goodMatches) <= 10: # Image highly distorted or too different
        return None, None

    dst_pts = np.float32([kpStart[m.queryIdx].pt for m in goodMatches[:matchPointNumber]]).reshape(-1, 1, 2)
    src_pts = np.float32([kpEnd[m.trainIdx].pt for m in goodMatches[:matchPointNumber]]).reshape(-1, 1, 2)
    matched_image = cv2.drawMatches(grayStart, kpStart, grayEnd, kpEnd, goodMatches[:matchPointNumber], grayEnd, flags=cv2.DRAW_MATCHES_FLAGS_NOT_DRAW_SINGLE_POINTS)

    h, status = cv2.findHomography(src_pts, dst_pts)
    aligned_image = cv2.warpPerspective(grayEnd, h, (origImage.shape[1], origImage.shape[0]))

    return matched_image, aligned_image


def cropImage(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # Conversion de l'image en niveaux de gris

    _, thresh = cv2.threshold(gray, 240, 255, cv2.THRESH_BINARY) # Binarisation et seuillage de l'image pour obtenir des contours propres
    thresh = 255 - thresh # Utiliser l'image en noir avec les éléments blancs permet de ne pas sélectionner les bords de l'image en tant que contour

    # cv2.RETR_EXTERNAL permet de ne garde que les contours les plus externes (utiliser cv2.RETR_TREE pour avoir tous les contours)
    # cv2.CHAIN_APPROX_SIMPLE permet un calcul plus rapide par la retenue des coins uniquement (cv2.CHAIN_APPROX_NONE pour avoir tous les points des contours)
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    max_contour = max(contours, key=cv2.contourArea) # Obtention du contour le plus grand
    # mask = np.zeros_like(thresh) # Création d'un masque (image noire)
    # cv2.drawContours(mask, [max_contour], -1, (255, 255, 255), thickness=cv2.FILLED) # Tracé de la zone d'intérêt en blanc dans le masque
    # result = cv2.bitwise_and(image, image, mask=mask) # Ajout de l'image originale dans la zone blanche

    x, y, w, h = cv2.boundingRect(max_contour) # Obtention des coordonnées d'encadrement du contour
    cropped_image = image[y:y+h, x:x+w] # Rognage de l'image

    return cropped_image


def tryEasyOCR(subImages: list, levenshteinGoals: list, showDiffs: bool):
    distances = []
    reader = easyocr.Reader(["fr"], gpu=False)
    for index in range(0, len(subImages)):
        if len(reader.detect(subImages[index])[0][0]) == 0:
            distance = LevenshteinDistance("", levenshteinGoals[index])
            distances.append(distance)
            if showDiffs:
                print("NOTHING -> " + levenshteinGoals[index] + ": " + str(distance))
            continue

        texts = reader.recognize(subImages[index])
        trial = ""
        for text in texts:
            trial += text[1] + " "
        trial = trial[:len(trial)-1]

        # Traitement des espaces rajoutés
        # if " " not in levenshteinGoals[index]:
        #     trial = trial.replace(" ", "") # S'il n'y a pas d'espace dans l'objectif mais qu'il y en a dans la détection, on les efface

        # Traitement des caractères similaires oO0
        # if "o" in trial or "O" in trial or "0" in trial:
        #     trial = tryLevenshteinOo0(trial, levenshteinGoals[index])

        # Traitement des variations dans la casse
        # upperAndLower = False
        # for char in trial:
        #     if char.isupper() != trial[0]:
        #         upperAndLower = True
        # if upperAndLower:
        #     trial = tryLevenshteinMAJ(trial, levenshteinGoals[index])

        # Triatement des espaces multiples
        # if "  " in trial:
        #     trial = tryLevenshteinDoubleSpaces(trial, levenshteinGoals[index])

        # Lancement du calcul de distance entre la chaîne modifiée et l'objectif
        distance = LevenshteinDistance(trial, levenshteinGoals[index])

        distances.append(distance)
        if showDiffs:
            print(trial + " -> " + levenshteinGoals[index] + ": " + str(distance))

    return distances


def tryLevenshteinOo0(trial: str, goal: str):
    initialDistance = LevenshteinDistance(trial, goal)
    modifiedTrial = ""

    for i in range(len(trial)):
        if trial[i] == 'o' or trial[i] == 'O' or trial[i] == '0':
            if i == len(trial)-1:
                endString = ""
            else:
                endString = trial[i+1:]

            if LevenshteinDistance(trial[:i] + 'o' + endString, goal) < initialDistance:
                modifiedTrial += 'o'
            elif LevenshteinDistance(trial[:i] + 'O' + endString, goal) < initialDistance:
                modifiedTrial += 'O'
            elif LevenshteinDistance(trial[:i] + '0' + endString, goal) < initialDistance:
                modifiedTrial += '0'
            else:
                modifiedTrial += trial[i]
        else:
            modifiedTrial += trial[i]

    return modifiedTrial


def tryLevenshteinMAJ(trial: str, goal: str):
    initialDistance = LevenshteinDistance(trial, goal)
    modifiedTrial = ""

    for i in range(len(trial)):
        if i == len(trial)-1:
            endString = ""
        else:
            endString = trial[i+1:]

        if trial[i].isupper():
            if LevenshteinDistance(trial[:i] + str(trial[i]).lower() + endString, goal) < initialDistance:
                modifiedTrial += str(trial[i]).lower()
            else:
                modifiedTrial += trial[i]
        else:
            if LevenshteinDistance(trial[:i] + str(trial[i]).upper() + endString, goal) < initialDistance:
                modifiedTrial += str(trial[i]).upper()
            else:
                modifiedTrial += trial[i]

    return modifiedTrial


def tryLevenshteinDoubleSpaces(trial: str, goal: str):
    initialDistance = LevenshteinDistance(trial, goal)

    while "  " in trial:
        i = trial.find(" ")
        if i == len(trial)-1:
            endString = ""
        else:
            endString = trial[i+1:]

        if LevenshteinDistance(trial[:i] + endString, goal) < initialDistance:
            trial = trial[:i] + endString

    return trial


def detectInteger(trial: str):
    modifiedTrial = ""
    for i in range(len(trial)):
        if trial[i] in "0123456789":
            modifiedTrial += trial[i]
    return modifiedTrial


def detectNumber(trial: str):
    modifiedTrial = ""
    for i in range(len(trial)):
        if trial[i] in "0123456789.,":
            modifiedTrial += trial[i]
    return modifiedTrial
