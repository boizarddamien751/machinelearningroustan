import cv2
import easyocr
import numpy as np
from enum import Enum


class DescriptorType(Enum):
    SIFT = 1
    KAZE = 2
    ORB = 3
    SURF = 3


class MatchType(Enum):
    NORMAL = 1
    KNN = 2


class DataType(Enum):
    IMMATRICULATION = 1
    DATE = 2
    MOT = 3
    TEXTE = 4
    ENTIER = 5
    DECIMAL = 6
    SPECIAL_K = 7
    VIDE = 8


def LevenshteinDistance(a, b, memo=None):
    if memo is None:
        memo = {}

    if (a, b) in memo:
        return memo[(a, b)]

    if a == "":
        result = len(b)
    elif b == "":
        result = len(a)
    else:
        cost = 0 if a[-1] == b[-1] else 1
        result = min([
            LevenshteinDistance(a[:-1], b, memo) + 1,
            LevenshteinDistance(a, b[:-1], memo) + 1,
            LevenshteinDistance(a[:-1], b[:-1], memo) + cost
        ])

    memo[(a, b)] = result
    return result


def homographyTransformation(origImage, endImage, matchPointNumber: int, descriptor: DescriptorType, matcher: MatchType):
    grayStart = cv2.cvtColor(origImage, cv2.COLOR_BGR2GRAY)
    grayEnd = cv2.cvtColor(endImage, cv2.COLOR_BGR2GRAY)

    # TODO : Mieux comprendre les algorithmes de détection et d'apparairage et notamment leurs paramètres
    try:
        if descriptor == DescriptorType.SIFT:
            descriptorAlgorithm = cv2.SIFT_create()
        elif descriptor == DescriptorType.KAZE:
            descriptorAlgorithm = cv2.KAZE_create()
        elif descriptor == DescriptorType.ORB:
            descriptorAlgorithm = cv2.ORB_create()
        elif descriptor == DescriptorType.SURF:
            descriptorAlgorithm = cv2.SURF_create()
        else:
            return None, None

        kpStart, desStart = descriptorAlgorithm.detectAndCompute(grayStart, None)
        kpEnd, desEnd = descriptorAlgorithm.detectAndCompute(grayEnd, None)
    except cv2.error as e:
        raise e

    # TODO : Chercher d'autres algorithmes d'apparairage de point entre deux images et approfondir l'utilisation de ceux-ci
    if matcher == MatchType.NORMAL:
        bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=True)
        matches = bf.match(desStart, desEnd)
        goodMatches = sorted(matches, key=lambda x: x.distance)
    elif matcher == MatchType.KNN:
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(np.asarray(desStart, np.float32), np.asarray(desEnd, np.float32), k=2)

        goodMatches = []
        for m, n in matches:
            if m.distance < 0.7 * n.distance:
                goodMatches.append(m)
        goodMatches = sorted(goodMatches, key=lambda x: x.distance)
    else:
        return None, None

    if len(goodMatches) <= 10: # Image highly distorted or too different
        return None, None

    dst_pts = np.float32([kpStart[m.queryIdx].pt for m in goodMatches[:matchPointNumber]]).reshape(-1, 1, 2)
    src_pts = np.float32([kpEnd[m.trainIdx].pt for m in goodMatches[:matchPointNumber]]).reshape(-1, 1, 2)
    matched_image = cv2.drawMatches(grayStart, kpStart, grayEnd, kpEnd, goodMatches[:matchPointNumber], grayEnd, flags=cv2.DRAW_MATCHES_FLAGS_NOT_DRAW_SINGLE_POINTS)

    h, status = cv2.findHomography(src_pts, dst_pts)
    aligned_image = cv2.warpPerspective(grayEnd, h, (origImage.shape[1], origImage.shape[0]))

    return matched_image, aligned_image


def cropImage(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) # Conversion de l'image en niveaux de gris

    _, thresh = cv2.threshold(gray, 240, 255, cv2.THRESH_BINARY) # Binarisation et seuillage de l'image pour obtenir des contours propres
    thresh = 255 - thresh # Utiliser l'image en noir avec les éléments blancs permet de ne pas sélectionner les bords de l'image en tant que contour

    # cv2.RETR_EXTERNAL permet de ne garde que les contours les plus externes (utiliser cv2.RETR_TREE pour avoir tous les contours)
    # cv2.CHAIN_APPROX_SIMPLE permet un calcul plus rapide par la retenue des coins uniquement (cv2.CHAIN_APPROX_NONE pour avoir tous les points des contours)
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    max_contour = max(contours, key=cv2.contourArea) # Obtention du contour le plus grand
    # mask = np.zeros_like(thresh) # Création d'un masque (image noire)
    # cv2.drawContours(mask, [max_contour], -1, (255, 255, 255), thickness=cv2.FILLED) # Tracé de la zone d'intérêt en blanc dans le masque
    # result = cv2.bitwise_and(image, image, mask=mask) # Ajout de l'image originale dans la zone blanche

    x, y, w, h = cv2.boundingRect(max_contour) # Obtention des coordonnées d'encadrement du contour
    cropped_image = image[y:y+h, x:x+w] # Rognage de l'image

    return cropped_image


def detectGrayCard(image: np.array, zoneConfigs: list, saveSubImages: bool = False):
    detectedTexts = []
    reader = easyocr.Reader(["fr"], gpu=False)

    for zoneConfig in zoneConfigs:
        subImage = image[zoneConfig[3][0]:zoneConfig[3][1], zoneConfig[3][2]:zoneConfig[3][3]]
        subImage = cv2.resize(subImage, (image.shape[1], int(image.shape[1] * subImage.shape[0] / subImage.shape[1])), interpolation=cv2.INTER_LINEAR)
        if saveSubImages:
            cv2.imwrite("images/all/" + zoneConfig[0] + ".jpg", subImage)

        if len(reader.detect(subImage)[0][0]) == 0 or zoneConfig[1] == DataType.VIDE:
            detectedTexts.append("")
            continue

        texts = reader.recognize(subImage)
        text = ""
        for element in texts:
            text += element[1] + " "
        text = text[:len(text)-1]

        featuresActivation = zoneConfig[2] if zoneConfig[2] is not None else {
            "Upper": False,
            "Lower": False,
            "NoSpace": False,
            "NoDoubleSpace": False,
            "ChangeOTo0": False,
            "Change0ToO": False,
            "ChangeZTo2": False
        }
        UpperDataType = [DataType.IMMATRICULATION, DataType.DATE, DataType.MOT, DataType.TEXTE, DataType.ENTIER, DataType.DECIMAL]
        LowerDataType = [DataType.SPECIAL_K]
        NoSpaceDataType = [DataType.IMMATRICULATION, DataType.DATE, DataType.MOT, DataType.ENTIER, DataType.DECIMAL, DataType.SPECIAL_K]
        NoDoubleSpaceDataType = [DataType.TEXTE]
        ChangeOTo0DataType = [DataType.DATE, DataType.ENTIER, DataType.DECIMAL, DataType.SPECIAL_K]
        Change0ToODataType = []
        ChangeZTo2DataType = [DataType.SPECIAL_K]

        if zoneConfig[1] in UpperDataType or featuresActivation["Upper"]:
            text = PatchFeatures.Upper(text)

        if zoneConfig[1] in LowerDataType or featuresActivation["Lower"]:
            text = PatchFeatures.Lower(text)

        if zoneConfig[1] in NoSpaceDataType or featuresActivation["NoSpace"]:
            text = PatchFeatures.NoSpace(text)

        if zoneConfig[1] in NoDoubleSpaceDataType or featuresActivation["NoDoubleSpace"]:
            text = PatchFeatures.NoDoubleSpace(text)

        if zoneConfig[1] in ChangeOTo0DataType or featuresActivation["ChangeOTo0"]:
            text = PatchFeatures.ChangeOTo0(text)

        if zoneConfig[1] in Change0ToODataType or featuresActivation["Change0ToO"]:
            text = PatchFeatures.Change0ToO(text)

        if zoneConfig[1] in ChangeZTo2DataType or featuresActivation["ChangeZTo2"]:
            text = PatchFeatures.ChangeZTo2(text)

        # Traitement spécial de caractères similaires '0' et 'O' sur une immatriculation de la forme XX-000-XX
        if zoneConfig[1] == DataType.IMMATRICULATION and len(text) >= 9:
            twoFirstLetters = PatchFeatures.Change0ToO(text[:2])
            twoLastLetters = PatchFeatures.Change0ToO(text[-2:])
            threeCentralDigits = PatchFeatures.ChangeOTo0(text[2:7])
            text = twoFirstLetters + threeCentralDigits + twoLastLetters

        # Traitement des caractères filtrés selon le type de données
        if zoneConfig[1] == DataType.IMMATRICULATION:
            text = PatchFeatures.Filter(text, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-")
        elif zoneConfig[1] == DataType.DATE:
            text = PatchFeatures.Filter(text, "0123456789/")
        elif zoneConfig[1] == DataType.MOT:
            text = PatchFeatures.Filter(text, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/")
        elif zoneConfig[1] == DataType.TEXTE:
            text = PatchFeatures.Filter(text, "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/ ")
        elif zoneConfig[1] == DataType.ENTIER:
            text = PatchFeatures.Filter(text, "0123456789")
        elif zoneConfig[1] == DataType.DECIMAL:
            text = PatchFeatures.Filter(text, "0123456789,.")
        elif zoneConfig[1] == DataType.SPECIAL_K:
            text = PatchFeatures.Filter(text, "e0123456789*/")

        detectedTexts.append(text)

    return detectedTexts


class PatchFeatures(object):
    @staticmethod
    def Upper(text: str):
        return text.upper()

    @staticmethod
    def Lower(text: str):
        return text.lower()

    @staticmethod
    def NoSpace(text: str):
        return text.replace(" ", "")

    @staticmethod
    def NoDoubleSpace(text: str):
        while "  " in text:
            text = text.replace("  ", " ")
        return text

    @staticmethod
    def ChangeOTo0(text: str):
        return text.replace("O", "0")

    @staticmethod
    def Change0ToO(text: str):
        return text.replace("0", "O")

    @staticmethod
    def ChangeZTo2(text: str):
        return text.replace("Z", "2")

    @staticmethod
    def Filter(text: str, whitelist: str):
        modifiedText = ""
        for char in text:
            if char in whitelist:
                modifiedText += char
        return modifiedText
