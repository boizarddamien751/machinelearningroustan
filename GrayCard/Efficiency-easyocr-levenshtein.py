import cv2
import numpy as np

from UtilsTests import tryEasyOCR
from statistics import mean

model = cv2.imread('images/carte-grise-specimen.jpg')

grayModel = cv2.cvtColor(model, cv2.COLOR_BGR2GRAY)

kernel = np.ones((3, 3), np.uint8)
# kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (18, 18))
test_dilated = cv2.dilate(grayModel, kernel, iterations=2)
test_median_blured = cv2.medianBlur(test_dilated, 3)

test_substrate = abs(255 - cv2.subtract(test_median_blured, grayModel))

test_substrate_normalized = cv2.normalize(test_substrate,  None, 0, 255, cv2.NORM_MINMAX)
thresh_substrate = cv2.adaptiveThreshold(test_substrate_normalized, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 5, 20)
# _, thresh_substrate = cv2.threshold(test_substrate_normalized, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)


tested_image = test_substrate
# subImages = [tested_image[5:19, 157:245], tested_image[5:21, 267:395], tested_image[42:50, 48:116],
#              tested_image[41:53, 155:193], tested_image[42:50, 208:276], tested_image[53:67, 47:125],
#              tested_image[53:67, 185:271], tested_image[71:85, 57:111], tested_image[103:117, 57:93],
#              tested_image[151:165, 25:53], tested_image[151:165, 67:95], tested_image[151:167, 125:229],
#              tested_image[151:167, 233:329], tested_image[171:187, 25:55], tested_image[171:187, 83:161],
#              tested_image[209:223, 81:109], tested_image[209:223, 115:143], tested_image[209:223, 149:227],
#              tested_image[239:253, 57:101], tested_image[239:253, 107:151], tested_image[241:255, 157:185],
#              tested_image[241:255, 191:235], tested_image[271:285, 57:109], tested_image[299:313, 23:45],
#              tested_image[299:315, 55:117], tested_image[321:335, 23:45], tested_image[319:335, 57:117],
#              tested_image[337:351, 307:339], tested_image[337:351, 349:453], tested_image[361:375, 23:45],
#              tested_image[361:375, 55:109], tested_image[361:377, 359:505], tested_image[377:391, 57:91],
#              tested_image[377:391, 149:171], tested_image[377:391, 181:217], tested_image[377:391, 307:343],
#              tested_image[395:411, 55:91], tested_image[397:411, 151:173], tested_image[397:411, 181:217],
#              tested_image[429:443, 55:219], tested_image[441:459, 21:43], tested_image[445:459, 55:91],
#              tested_image[445:459, 149:171], tested_image[445:459, 275:297], tested_image[447:461, 369:391],
#              tested_image[461:475, 55:91], tested_image[461:475, 149:171], tested_image[463:477, 275:297],
#              tested_image[463:477, 369:391], tested_image[479:495, 55:91], tested_image[481:495, 149:171],
#              tested_image[481:495, 181:209], tested_image[495:511, 55:107], tested_image[497:511, 113:159],
#              tested_image[497:511, 165:276], tested_image[476:560, 20:46], tested_image[511:527, 55:107],
#              tested_image[513:527, 149:171], tested_image[516:524, 360:396], tested_image[516:524, 410:518],
#              tested_image[528:536, 396:518], tested_image[529:543, 149:171], tested_image[540:548, 418:518],
#              tested_image[545:559, 149:171], tested_image[545:561, 181:233], tested_image[578:602, 256:366],
#              tested_image[507:619, 540:556], tested_image[601:615, 55:141], tested_image[612:650, 20:44],
#              tested_image[649:667, 21:43], tested_image[665:683, 21:43], tested_image[768:774, 452:514],
#              tested_image[771:787, 27:185], tested_image[809:825, 35:87], tested_image[811:825, 149:235],
#              tested_image[831:847, 35:129], tested_image[851:867, 33:179], tested_image[871:887, 33:95],
#              tested_image[903:917, 33:87], tested_image[939:953, 33:69], tested_image[971:991, 23:498],
#              tested_image[997:1017, 23:496]]

# levenshteinGoals = [
#     "CERTIFICAT",
#     "IMMATRICULATION",
#     "Immatriculation",
#     "Date de",
#     "immatriculation",
#     "AB-123-CD",
#     "05/01/1998",
#     "DUPONT",
#     "YVES",
#     "C.4a",
#     "EST",
#     "PROPRIETAIRE",
#     "DU VEHICULE",
#     "C.4.1",
#     "DELAROCHE",
#     "RUE",
#     "DES",
#     "ROITELETS",
#     "59169",
#     "FERIN",
#     "LES",
#     "BAINS",
#     "FRANCE",
#     "D.1",
#     "MARQUES",
#     "D.2",
#     "VERSION",
#     "D.2.1",
#     "WCT5432PY313",
#     "D.3",
#     "MODELE",
#     "VFSIV2009ASIV2009",
#     "1915",
#     "F.2",
#     "1915",
#     "1915",
#     "3030",
#     "G.1",
#     "1307",
#     "e2*2001/116*0317*02",
#     "P.1",
#     "1900",
#     "P.2",
#     "P.3",
#     "P.6",
#     "0,06",
#     "S.1",
#     "S.2",
#     "U.1",
#     "3000",
#     "V.7",
#     "155",
#     "VISITE",
#     "AVANT",
#     "LE 06/07/2011",
#     "U.2"
#     "X.1"
#     "Y.1"
#     "Y.3"
#     "Y.5",
#     "178,35",
#     "Y.2",
#     "Ministère",
#     "l'intérieur et par délégation",
#     "sous-directeur de la circulation",
#     "Y.4",
#     "et de la sécurité routières",
#     "Y.6",
#     "178,35",
#     "SPECIMEN",
#     "2M26000534680",
#     "04/12/2009",
#     "Z.1"
#     "Z.2",
#     "Z.3",
#     "Z.4",
#     "COUPON DÉTACHA",
#     "Certificat d'immatriculation",
#     "AB-123",
#     "04/12/2009",
#     "2009AS05284",
#     "VFSIV2009ASIV2009",
#     "MARQUES",
#     "DUPONT",
#     "YVES",
#     "CRFRAAB123CDOVFSIV2009ASIV200929801059VP<<<<",
#     "CI<<MARQUES<<<<<<<MODELE<<<<<<<2009AS0528402"
# ]

levenshteinGoals = [
    "AB-123-CD", "05/01/1998", "DUPONT",
    "YVES", "EST LE PROPRIETAIRE DU VEHICULE", "2 DELAROCHE",
    "27 RUE DES ROITELETS", "59169 FERIN LES BAINS", "FRANCE",
    "MARQUES", "VERSION", "MCT5432PY315",
    "MODELE", "VFSIV2009ASIV2009", "1915",
    "1915", "1915", "3030",
    "1307", "M1", "VP",
    "CI", "CI", "e2*2001/116*0317*02",
    "1900", "90", "GO",
    "6", "0,06", "5",
    "", "77", "3000",
    "155", "", "VISITE AVANT LE 06/07/2011",
    "178,35", "", "",
    "4", "25", "178,35",
    "", "04/12/2009", "",
    "", "", "",
]

subImages = [
    tested_image[50:70, 45:130], tested_image[50:70, 182:278], tested_image[68:88, 53:377],
    tested_image[98:118, 53:377], tested_image[148:168, 63:452], tested_image[168:188, 65:452],
    tested_image[206:226, 57:461], tested_image[237:257, 57:461], tested_image[269:289, 57:237],
    tested_image[299:319, 55:459], tested_image[315:335, 55:459], tested_image[335:355, 348:508],
    tested_image[359:379, 55:325], tested_image[359:379, 358:531], tested_image[375:395, 55:110],
    tested_image[375:395, 181:236], tested_image[375:395, 307:362], tested_image[393:413, 55:110],
    tested_image[393:413, 181:236], tested_image[408:428, 55:110], tested_image[408:428, 181:236],
    tested_image[408:428, 307:362], tested_image[408:428, 400:455], tested_image[425:445, 55:500],
    tested_image[441:461, 55:110], tested_image[441:461, 181:236], tested_image[441:461, 307:362],
    tested_image[441:461, 400:455], tested_image[457:477, 55:110], tested_image[457:477, 181:236],
    tested_image[457:477, 307:362], tested_image[457:477, 400:455], tested_image[476:496, 55:110],
    tested_image[476:496, 181:236], tested_image[476:496, 307:362], tested_image[493:513, 55:515],
    tested_image[509:529, 55:110], tested_image[509:529, 181:236], tested_image[525:545, 55:110],
    tested_image[525:545, 181:236], tested_image[540:560, 55:110], tested_image[540:560, 181:236],
    tested_image[582:602, 55:155], tested_image[597:617, 55:155], tested_image[612:632, 55:155],
    tested_image[627:647, 55:155], tested_image[642:662, 55:155], tested_image[657:677, 55:155]
]

for i in range(len(subImages)):
    subImages[i] = cv2.resize(subImages[i], (tested_image.shape[1], int(tested_image.shape[1] * subImages[i].shape[0] / subImages[i].shape[1])), interpolation=cv2.INTER_LINEAR)
    # _, subImages[i] = cv2.threshold(subImages[i], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imwrite("images/all/" + str(i) + ".jpg", subImages[i])

distances = tryEasyOCR(subImages, levenshteinGoals, True)

if len(distances) > 0:
    print("Total distance : ", sum(distances))
    print("Distance mean : ", mean(distances))


