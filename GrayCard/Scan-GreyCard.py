# https://github.com/x4nth055/pythoncode-tutorials/tree/master/machine-learning/sift

from Utils import *

model = cv2.imread('images/carte-grise-specimen.jpg')
test = cv2.imread('images/carte-grise-test.jpg')

test_croped = cropImage(test)

# 50 SIFT NORMAL 99%
# 50 KAZE NORMAL 100%
# 50 ORB NORMAL 99%
# 50 SURF NORMAL 99%
# 50 SIFT KNN 100%
# 50 KAZE KNN 100%
# 50 ORB KNN 60%
# 50 SURF KNN 60%
keypoints, test_aligned = homographyTransformation(model, test_croped, 50, DescriptorType.KAZE, MatchType.KNN)
if keypoints is None or test_aligned is None:
    print("Echec de l'opération")
    exit()

print("Pourcentage de réussite de l'alignement :", 100 - (np.sum(test_aligned == 0) / test_aligned.size) * 100, "%")

# Dilatation de l'image et ajout d'un flou pour obtenir le fond
kernel = np.ones((3, 3), np.uint8)
test_dilated = cv2.dilate(test_aligned, kernel, iterations=2)
test_median_blured = cv2.medianBlur(test_dilated, 3)

# Soustraction du fond sur l'image initiale pour obtenir un texte avec le moins de fond possible
test_substrate = abs(255 - cv2.subtract(test_median_blured, test_aligned))

# Normalisation de l'image et binarisation de celle-ci
test_substrate_normalized = cv2.normalize(test_substrate,  None, 0, 255, cv2.NORM_MINMAX)
thresh_substrate = cv2.adaptiveThreshold(test_substrate_normalized, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 5, 20)
# _, thresh_substrate = cv2.threshold(test_substrate_normalized, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

cv2.imwrite("images/carte-grise-keypoints.jpg", keypoints)
cv2.imwrite("images/carte-grise-test_croped.jpg", test_croped)
cv2.imwrite("images/carte-grise-test-aligned.jpg", test_aligned)
cv2.imwrite("images/carte-grise-test-dilated.jpg", test_dilated)
cv2.imwrite("images/carte-grise-test-median-blured.jpg", test_median_blured)
cv2.imwrite("images/carte-grise-test-substrate.jpg", test_substrate)
cv2.imwrite("images/carte-grise-test-thresh.jpg", thresh_substrate)

zoneConfig = [
    ("A", DataType.IMMATRICULATION, None, (50, 70, 45, 130)),
    ("B", DataType.DATE, None, (50, 70, 183, 278)),
    ("C.1 Nom", DataType.MOT, None, (68, 88, 53, 377)),
    ("C.1 Prénom", DataType.MOT, None, (98, 118, 53, 377)),
    ("C.4a", DataType.TEXTE, None, (148, 168, 63, 452)),
    ("C.4.1", DataType.TEXTE, None, (168, 188, 65, 452)),
    ("C.3 Adresse", DataType.TEXTE, None, (206, 226, 57, 461)),
    ("C.3 Commune", DataType.TEXTE, None, (237, 257, 57, 461)),
    ("C.3 Pays", DataType.MOT, None, (269, 289, 57, 237)),
    ("D.1", DataType.TEXTE, None, (299, 319, 55, 459)),
    ("D.2", DataType.TEXTE, None, (315, 335, 55, 459)),
    ("D.2.1", DataType.MOT, None, (335, 355, 348, 508)),
    ("D.3", DataType.TEXTE, None, (359, 379, 55, 325)),
    ("E", DataType.MOT, None, (359, 379, 358, 531)),
    ("F.1", DataType.ENTIER, None, (375, 395, 55, 110)),
    ("F.2", DataType.ENTIER, None, (375, 395, 181, 236)),
    ("F.3", DataType.ENTIER, None, (375, 395, 307, 362)),
    ("G", DataType.ENTIER, None, (393, 413, 55, 110)),
    ("G.1", DataType.ENTIER, None, (393, 413, 181, 236)),
    ("J", DataType.MOT, None, (408, 428, 55, 110)),
    ("J.1", DataType.MOT, None, (408, 428, 181, 236)),
    ("J.2", DataType.MOT, None, (408, 428, 307, 362)),
    ("J.3", DataType.MOT, None, (408, 428, 400, 455)),
    ("K", DataType.SPECIAL_K, None, (425, 445, 55, 500)),
    ("P.1", DataType.ENTIER, None, (441, 461, 55, 110)),
    ("P.2", DataType.ENTIER, None, (441, 461, 181, 236)),
    ("P.3", DataType.MOT, None, (441, 461, 307, 362)),
    ("P.6", DataType.ENTIER, None, (441, 461, 400, 455)),
    ("Q", DataType.DECIMAL, None, (457, 477, 55, 110)),
    ("S.1", DataType.ENTIER, None, (457, 477, 181, 236)),
    ("S.2", DataType.ENTIER, None, (457, 477, 307, 362)),
    ("U.1", DataType.ENTIER, None, (457, 477, 400, 455)),
    ("U.2", DataType.ENTIER, None, (476, 496, 55, 110)),
    ("V.7", DataType.ENTIER, None, (476, 496, 181, 236)),
    ("V.9", DataType.MOT, None, (476, 496, 307, 362)),
    ("X.1", DataType.TEXTE, None, (493, 513, 55, 515)),
    ("Y.1", DataType.DECIMAL, None, (509, 529, 55, 110)),
    ("Y.2", DataType.DECIMAL, None, (509, 529, 181, 236)),
    ("Y.3", DataType.DECIMAL, None, (525, 545, 55, 110)),
    ("Y.4", DataType.DECIMAL, None, (525, 545, 181, 236)),
    ("Y.5", DataType.DECIMAL, None, (540, 560, 55, 110)),
    ("Y.6", DataType.DECIMAL, None, (540, 560, 181, 236)),
    ("H", DataType.DATE, None, (582, 602, 55, 155)),
    ("I", DataType.DATE, None, (597, 617, 55, 155)),
    ("Z.1", DataType.TEXTE, None, (612, 632, 55, 155)),
    ("Z.2", DataType.TEXTE, None, (627, 647, 55, 155)),
    ("Z.3", DataType.TEXTE, None, (642, 662, 55, 155)),
    ("Z.4", DataType.TEXTE, None, (657, 677, 55, 155))
]

results = detectGrayCard(test_substrate, zoneConfig, True)

for index in range(len(results)):
    print(zoneConfig[index][0] + ": " + results[index])
