import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import SGDRegressor
from sklearn.datasets import make_regression

x, y = make_regression(n_samples=1000, n_features=1, noise=10, n_targets=1)

model_from_scikitlearn = SGDRegressor(max_iter=1000)
model_from_scikitlearn.fit(x, y)

plt.plot(list(np.transpose(x))[0], np.transpose(y), 'ro', markersize=1)
x = np.linspace(np.min(x), np.max(x), 1000)
plt.plot(x, model_from_scikitlearn.predict(np.transpose([x])))
plt.show()
