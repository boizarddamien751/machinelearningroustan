import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import make_regression

ordre = 3
x, y = make_regression(n_samples=1000, n_features=1, noise=10, n_targets=1)
y = y**ordre

m = float(len(x))

tupleX = []
for i in range(ordre, -1, -1):
    tupleX.append(x**i)
tupleX = tuple(tupleX)

X = np.hstack(tupleX)
Y = np.transpose([y])

O = []
for i in range(ordre+1):
    O.append([1])

FX = []
alpha = 0.001
erreurMoyenne = []

iter = 1
maxIter = 5000

while iter < maxIter:
    FX = np.dot(X, O) # F(X) = X * O

    erreurMoyenne.append((1.0 / (2.0 * m)) * sum(np.square(FX - Y)))

    derivee = (1.0 / m) * np.dot(np.transpose(X), FX - Y)
    O -= derivee * alpha

    iter += 1

plt.figure(figsize=(13, 5))

plt.subplot(121)
plt.xlabel("Superficie")
plt.ylabel("Prix")

x_parabola = np.linspace(np.min(x), np.max(x), 1000)
x_parabola = np.transpose([x_parabola])
tupleX = []
for i in range(ordre, -1, -1):
    tupleX.append(x_parabola**i)
tupleX = tuple(tupleX)
y_parabola = np.dot(np.hstack(tupleX), O)
plt.plot(x_parabola, y_parabola)

plt.plot(x, y, 'ro', markersize=2)

plt.subplot(122)
plt.xlabel("Itération")
plt.ylabel("Gradient")

plt.plot(np.linspace(1, maxIter-1, maxIter-1), erreurMoyenne)

plt.show()
