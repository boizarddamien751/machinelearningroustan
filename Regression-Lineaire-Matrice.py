import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import make_regression

x, y = make_regression(n_samples=1000, n_features=1, noise=10, n_targets=1)

m = float(len(x))

X = np.hstack((x, np.ones((int(m), 1))))
Y = np.transpose([y])

O = [[1], [1]]
FX = []
alpha = 0.001
erreurMoyenne = []

iter = 1
maxIter = 5000

while iter < maxIter:
    FX = np.dot(X, O) # F(X) = X * O

    erreurMoyenne.append((1.0 / (2.0*m)) * sum(np.square(FX - Y)))

    derivee = (1.0 / m) * np.dot(np.transpose(X), FX - Y)
    O -= derivee * alpha

    iter += 1

plt.figure(figsize=(13, 5))

plt.subplot(121)
plt.xlabel("Superficie")
plt.ylabel("Prix")

plt.plot([np.min(x), np.max(x)], np.transpose(np.matmul([[np.min(x), 1], [np.max(x), 1]], O))[0])
# plt.plot(x, FX)
plt.plot(x, y, 'ro', markersize=2)

plt.subplot(122)
plt.xlabel("Itération")
plt.ylabel("Gradient")

plt.plot(np.linspace(1, maxIter-1, maxIter-1), erreurMoyenne)

plt.show()
