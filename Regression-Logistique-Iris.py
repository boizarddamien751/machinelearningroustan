import matplotlib.pyplot as plt
import numpy as np
import math
from sklearn import datasets


def sigmoid(x, p):
    return 1 / (1 + pow(math.e, -p*x))


def sigmoidArray(array):
    return np.array([[sigmoid(data, 1) for data in row] for row in array])


iris = datasets.load_iris()
x = iris.data[:, 0][:list(iris.target).index(2)]
y = iris.target[:list(iris.target).index(2)]

# fig, ax = plt.subplots()
# scatter = ax.scatter(iris.data[:, 0], iris.data[:, 1], c=iris.target)
# ax.set(xlabel=iris.feature_names[0], ylabel=iris.feature_names[1])
# ax.legend(scatter.legend_elements()[0], iris.target_names, loc="lower right", title="Classes")

m = len(x)

X = np.hstack(
    (
        np.array([x]).transpose(),
        np.ones((m, 1))
    )
)
Y = np.array([y]).transpose()

O = [[1], [1]]
FX = []
alpha = 0.001
erreurMoyenne = []

iter = 1
maxIter = 5000

while iter < maxIter:
    FX = np.dot(X, O) # F(X) = X * O

    # erreurMoyenne.append((1.0 / (2.0*m)) * sum(sigmoidArray(FX) - Y))

    derivee = (1.0 / float(m)) * np.dot(np.transpose(X), sigmoidArray(FX) - Y)
    O -= derivee * alpha

    iter += 1

plt.plot(x, y, 'ro')
x = np.linspace(4, 7, 1000)
plt.plot(x, [sigmoid(i * list(O)[0][0] + list(O)[1][0], 1) for i in x])

plt.show()
