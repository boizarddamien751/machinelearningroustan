from math import cos, sin
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Slider

x = np.linspace(-6, 6, 1000)
xId = 0


def f(t):
    return cos(t) + t


def df(t):
    return -sin(t) + 1.0


def tangente(t):
    global x
    return [f(t) + (i - t) * df(t) for i in x]


def vertical(t):
    y = np.linspace(0, f(t), 1000)
    return [t for i in y], y


def showTangente(t):
    ax.plot(x, tangente(t), color="red", alpha=0.3)


def showVertical(t):
    global xId
    xVertical, yVertical = vertical(t)
    ax.plot(xVertical, yVertical, '--', color="black", alpha=0.5)
    ax.text(t-0.2, -0.3, str('x') + str(xId))
    xId += 1

def update(val):
    global xId
    ax.clear()

    ax.axhline(0, color="blue", alpha=0.3)
    ax.axvline(0, color="blue", alpha=0.3)
    ax.set_xlim(-6, 6)
    ax.set_ylim(-4, 4)

    ax.plot(x, [f(i) for i in x])
    ax.plot(x, [df(i) for i in x], ':', alpha=0.3)

    xId = 0
    xn = val
    for i in range(5):
        showTangente(xn)
        showVertical(xn)
        xn -= (f(xn) / df(xn))


fig, ax = plt.subplots()
ax.plot([], [])
# fig.set_figwidth(5)
# fig.set_figheight(5)

slider_ax = plt.axes((0.2, 0.02, 0.65, 0.03))
slider_width = Slider(slider_ax, 'x0', -6.0, 6.0, valinit=2.0)

slider_width.on_changed(update)
update(2.0)
plt.show()