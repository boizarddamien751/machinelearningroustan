import matplotlib.pyplot as plt
import numpy as np
import math


def sigmoid(x, p):
    return 1 / (1 + pow(math.e, -p*x))


for p in range(1, 20):
    x_shown = np.linspace(-10, 10, 1000)
    plt.plot(x_shown, [sigmoid(data, p) for data in x_shown])
plt.show()
