import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
from sklearn.datasets import make_regression


def myfunc(xData):
    return slope * xData + intercept


x, y = make_regression(n_samples=1000, n_features=1, noise=10, n_targets=1)
x = np.transpose(x)
y = np.transpose(y)

slope, intercept, r, p, std_err = stats.linregress(x, y)

plt.plot(list(x)[0], y, 'ro', markersize=1)
x = np.linspace(np.min(x), np.max(x), 1000)
plt.plot(x, list(map(myfunc, x)))
plt.show()
