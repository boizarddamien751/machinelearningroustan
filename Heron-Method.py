import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from matplotlib.patches import Rectangle

a = 1.0
x = a / 2.0
y = a / x


def update(val):
    global x, y, ax
    x = val
    y = a / x

    ax.clear()
    for i in range(3):
        rect = Rectangle((0, 0), x, y, alpha=0.5)
        ax.add_patch(rect)
        ax.text(x, y, str(x))
        x = (x + (a / x)) / 2.0
        y = a / x

    ax.set_xlim(0, 2.1)
    ax.set_ylim(0, 2.1)
    fig.canvas.draw_idle()


fig, ax = plt.subplots()
ax.plot([], [])
fig.set_figwidth(5)
fig.set_figheight(5)

slider_ax = plt.axes((0.2, 0.02, 0.65, 0.03))
slider_width = Slider(slider_ax, 'x', 0.5, 1.0, valinit=x)

slider_width.on_changed(update)
update(0.5)
plt.show()
