import matplotlib.pyplot as plt
import numpy as np
import math


def sigmoid(x, p):
    return 1 / (1 + pow(math.e, -p*x))


def sigmoidArray(array):
    return np.array([[sigmoid(data, 1) for data in row] for row in array])


plt.figure(figsize=(12, 5))

# plt.subplot(121)
# x = np.linspace(0.01, 10, 1000)
# plt.plot(x, [math.log(data) for data in x])
#
# plt.subplot(122)
# x = np.linspace(-10, 10, 1000)
# plt.plot(x, [-math.log(sigmoid(data, 1)) for data in x])
# plt.plot(x, [-math.log(1-sigmoid(data, 1)) for data in x])

# La mesure de l'erreur MSE ne convient pas ici et suggèrerait l'utilisation de la fonction carré pour le calcul de la moyenne de l'erreur
# Nous utiliserons ici le logarithme à la place

plt.subplot(111)
x = np.array([[1, 2, 3, 4]]).transpose()
y = np.array([[1, 1, 0, 0]])

m = float(len(x))

X = np.hstack((x, np.ones((int(m), 1))))
Y = np.transpose(y)

O = [[1], [1]]
FX = []
alpha = 0.001
erreurMoyenne = []

iter = 1
maxIter = 5000

while iter < maxIter:
    FX = np.dot(X, O) # F(X) = X * O

    # erreurMoyenne.append((1.0 / (2.0*m)) * sum(sigmoidArray(FX) - Y))

    derivee = (1.0 / m) * np.dot(np.transpose(X), sigmoidArray(FX) - Y)
    O -= derivee * alpha

    iter += 1

plt.plot(x.transpose()[0], y[0], 'ro')
x = np.linspace(0, 4, 1000)
plt.plot(x, [sigmoid(i * list(O)[0][0] + list(O)[1][0], 1) for i in x])

plt.show()
